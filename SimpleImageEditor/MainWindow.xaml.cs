﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using Point = System.Windows.Point;
using SimpleImageEditor.ViewModel;
using Microsoft.Win32;
using SimpleImageEditor.Model;
using SimpleImageEditor.View;

namespace SimpleImageEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Viewmodel Vm;
        private ToolbarWindow toolWindow;

        public MainWindow()
        {
            InitializeComponent();
            this.Vm = Viewmodel.GetViewModel();
            this.DataContext = Vm;

            Vm.SetWindowElements(this, theGrid, selectionBox, selected, img, redSlider, greenSlider, blueSlider);

            Vm.OpenFileLoader();
        }


        private void onButtonClick(object sender, RoutedEventArgs e)
        {
            Vm.OnButtonClick(sender, e);
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Vm.Image_MouseDown(sender, e);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Vm.Grid_MouseUp(sender, e);
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            Vm.Grid_MouseMove(sender, e);
        }

        private void Grid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Vm.Grid_MouseRightButtonDown(sender, e);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            Vm.OnKeyDown(sender, e);
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Vm.MainWindow_SizeChanged(sender, e);
        }

        private void mainWindow_Closed(object sender, EventArgs e)
        {
            if (toolWindow != null)
                toolWindow.Close();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            toolWindow = new ToolbarWindow(this.Left + this.ActualWidth, this.Top);
            toolWindow.Show();
        }
    }
}
