﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Rectangle = System.Windows.Shapes.Rectangle;
using Image = System.Windows.Controls.Image;
using Grid = System.Windows.Controls.Grid;
using Point = System.Windows.Point;
using SimpleImageEditor.Model;

namespace SimpleImageEditor.ViewModel
{
    public static class ImageManipulator
    {
        public static Bitmap LoadIamge(string fileName)
        {
            Bitmap bitmap;
            try
            {
                using (Stream bmpStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(bmpStream);

                    bitmap = new Bitmap(image);
                }
            }
            catch (Exception)
            {
                throw new NotValidFileException();
            }
            return bitmap;
        }

        public static BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);

                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static void AddShade(bool[] rgbAcitve, int[] values, Bitmap bitmap, Window window, Grid grid, Image image, Rectangle selected)
        {
            int startX = 0, startY = 0, endX = bitmap.Width, endY = bitmap.Height;
            double scaleX = 1;
            double scaleY = 1;

            //use selection if available
            if (selected.Visibility == Visibility.Visible)
                SetStart(ref startX, ref startY, ref endX, ref endY, ref scaleX, ref scaleY, window, grid, image, selected);

            for (int i = startX; i < endX; i++)
                for (int j = startY; j < endY; j++)
                {
                    var pix = bitmap.GetPixel(i, j);
                    int r = pix.R, g = pix.G, b = pix.B;
                    if (rgbAcitve[0])
                    {
                        r = pix.R + values[0] > 255 ? 255 : pix.R + values[0];
                        r = r < 0 ? 0 : r;
                    }
                    if (rgbAcitve[1])
                    {
                        g = pix.G + values[1] > 255 ? 255 : pix.G + values[1];
                        g = g < 0 ? 0 : g;
                    }
                    if (rgbAcitve[2])
                    {
                        b = pix.B + values[2] > 255 ? 255 : pix.B + values[2];
                        b = b < 0 ? 0 : b;
                    }
                    bitmap.SetPixel(i, j, Color.FromArgb(pix.A, r, g, b));
                }
        }

        private static void SetStart(ref int startX, ref int startY, ref int endX, ref int endY, ref double scaleX, ref double scaleY, Window window, Grid grid, Image image, Rectangle selected)
        {
            int strtX = startX, strtY = startX, eX = endX, eY = endY;
            double scX = scaleX, scY = scaleY;

            window.Dispatcher.Invoke(new Action(() =>
            {
                scX = eX / image.ActualWidth;
                scY = eY / image.ActualHeight;

                Point topLeft = image.TransformToAncestor(grid).Transform(new Point(0, 0));
                Point rightBottom = new Point(topLeft.X + image.ActualWidth, topLeft.Y + image.ActualHeight);

                Point topLeftSelected = selected.TransformToAncestor(grid).Transform(new Point(0, 0));
                Point rightBottomSelected = new Point(topLeftSelected.X + selected.ActualWidth, topLeftSelected.Y + selected.ActualHeight);


                strtX = (int)((topLeftSelected.X - topLeft.X) * scX);
                eX = (int)((rightBottomSelected.X - topLeft.X) * scX);
                strtY = (int)((topLeftSelected.Y - topLeft.Y) * scY);
                eY = (int)((rightBottomSelected.Y - topLeft.Y) * scY);
            }));
            startX = strtX;
            startY = strtY;
            endX = eX;
            endY = eY;

            scaleX = scX;
            scaleY = scY;
        }

    }
}
