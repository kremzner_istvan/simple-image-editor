﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows;
using Point = System.Windows.Point;
using Rectangle = System.Windows.Shapes.Rectangle;
using Image = System.Windows.Controls.Image;
using Grid = System.Windows.Controls.Grid;
using Stretch = System.Windows.Media.Stretch;
using System.Windows.Input;
using System.Windows.Data;
using SimpleImageEditor.Model;
using Microsoft.Win32;

namespace SimpleImageEditor.ViewModel
{
    public class Viewmodel
    {
        #region Singleton
        private static Viewmodel VM;

        public static Viewmodel GetViewModel()
        {
            if (VM == null)
                VM = new Viewmodel();

            return VM;
        }
        #endregion

        public static string DEFAULTFILE = "pexels-photo.png";

        private Bitmap bitmap;
        private Bitmap originalBitmap;
        //private object lockObject;

        private bool redEnabled = true;
        private bool greenEnabled = true;
        private bool blueEnabled = true;

        private bool mouseDown = false; // Set to 'true' when mouse is held down.
        private System.Windows.Point mouseDownPos; // The point where the mouse button was clicked down.

        public MainWindow MainWin { private get; set; }
        public Grid TheGrid { private get; set; }
        public Rectangle Selected { private get; set; }
        public Rectangle SelectionBox { private get; set; }
        public Image Img { private get; set; }
        public Slider RedS { private get; set; }
        public Slider BlueS { private get; set; }
        public Slider GreenS { private get; set; }

        public String CurrentFile { get; set; }

        //public bool All

        public void SetWindowElements(MainWindow mainWindow, Grid theGrid, Rectangle selectionBox, Rectangle selected, Image img, Slider red, Slider green, Slider blue)
        {
            this.MainWin = mainWindow;
            this.TheGrid = theGrid;
            this.SelectionBox = selectionBox;
            this.Selected = selected;
            this.Img = img;
            this.RedS = red;
            this.BlueS = blue;
            this.GreenS = green;
        }

        public Bitmap ImgSource
        {
            get
            {
                return bitmap;
            }
        }

        private Viewmodel()
        {
            //lockObject = new object();
            CurrentFile = DEFAULTFILE;
            bitmap = ImageManipulator.LoadIamge(CurrentFile);
        }

        public void OpenFileLoader()
        {
            OpenFileDialog fileOpener = new OpenFileDialog();
            fileOpener.Filter = fileOpener.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";
            fileOpener.ShowDialog();

            this.CurrentFile = fileOpener.FileName;
            this.LoadFile();
        }

        private void LoadFile()
        {
            try
            {
                if (CurrentFile == "")
                    CurrentFile = DEFAULTFILE;

                bitmap = ImageManipulator.LoadIamge(CurrentFile);
            }
            catch (NotValidFileException)
            {
                MessageBox.Show("Not valid file, loaded default file.", "Bad format!");
                CurrentFile = DEFAULTFILE;
                LoadFile();
            }
            originalBitmap = (Bitmap)bitmap.Clone();
            RefreshImageBinding();
        }

        public void SaveFile()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.Flush();
                    bitmap.Save(saveFileDialog.FileName);
                }

                MessageBox.Show("File saved succesfully.", "Saved");
            }
        }

        public void RefreshImageBinding()
        {
            Img.GetBindingExpression(Image.SourceProperty).UpdateTarget();
        }

        public void RestoreImage()
        {
            bitmap = (Bitmap)originalBitmap.Clone();
            RefreshImageBinding();
        }

        public async void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int sign = 1;
            bool all = button.Name.ToLower().Contains("selected");

            bool[] enabled = { false, false, false };
            int[] values = { 0, 0, 0 };

            if (button.Name.ToLower().Contains("remove"))
                sign = -1;

            if ((all && redEnabled) || button.Name.ToLower().Contains("red"))
            {
                values[0] = (int)RedS.Value * sign;
                enabled[0] = true;
            }
            if ((all && greenEnabled) || button.Name.ToLower().Contains("green"))
            {
                values[1] = (int)GreenS.Value * sign;
                enabled[1] = true;
            }
            if ((all && blueEnabled) || button.Name.ToLower().Contains("blue"))
            {
                values[2] = (int)BlueS.Value * sign;
                enabled[2] = true;
            }

            await Task.Run(() => ImageManipulator.AddShade(enabled, values, bitmap, MainWin, TheGrid, Img, Selected));

            RefreshImageBinding();
        }

        public void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Selected.Visibility = Visibility.Collapsed;

            mouseDownPos = e.GetPosition(TheGrid);

            // Capture and track the mouse.
            mouseDown = true;
            TheGrid.CaptureMouse();

            // Initial placement of the drag selection box.         
            Canvas.SetLeft(SelectionBox, mouseDownPos.X);
            Canvas.SetTop(SelectionBox, mouseDownPos.Y);
            SelectionBox.Width = 0;
            SelectionBox.Height = 0;

            // Make the drag selection box visible.
            SelectionBox.Visibility = Visibility.Visible;
        }

        public void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Canvas.SetLeft(Selected, Canvas.GetLeft(SelectionBox));
            Canvas.SetTop(Selected, Canvas.GetTop(SelectionBox));
            Selected.Width = SelectionBox.Width;
            Selected.Height = SelectionBox.Height;
            Selected.Visibility = Visibility.Visible;

            // Release the mouse capture and stop tracking it.
            mouseDown = false;
            TheGrid.ReleaseMouseCapture();

            // Hide the drag selection box.
            SelectionBox.Visibility = Visibility.Collapsed;

            System.Windows.Point mouseUpPos = e.GetPosition(TheGrid);

            // TODO: 
            //
            // The mouse has been released, check to see if any of the items 
            // in the other canvas are contained within mouseDownPos and 
            // mouseUpPos, for any that are, select them!
            //
        }

        public void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                // When the mouse is held down, reposition the drag selection box.

                System.Windows.Point mousePosImg = e.GetPosition(Img);
                System.Windows.Point mousePos = e.GetPosition(TheGrid);

                //Move cursor back in the image bounds
                if (mousePosImg.X < 0)
                    mousePos.X += (-mousePosImg.X);
                else if (mousePosImg.X > Img.ActualWidth)
                    mousePos.X -= mousePosImg.X - Img.ActualWidth;
                if (mousePosImg.Y < 0)
                    mousePos.Y += (-mousePosImg.Y);
                else if (mousePosImg.Y > Img.ActualHeight)
                    mousePos.Y -= mousePosImg.Y - Img.ActualHeight;

                if (mouseDownPos.X < mousePos.X)
                {
                    Canvas.SetLeft(SelectionBox, mouseDownPos.X);
                    SelectionBox.Width = mousePos.X - mouseDownPos.X;
                }
                else
                {
                    Canvas.SetLeft(SelectionBox, mousePos.X);
                    SelectionBox.Width = mouseDownPos.X - mousePos.X;
                }

                if (mouseDownPos.Y < mousePos.Y)
                {
                    Canvas.SetTop(SelectionBox, mouseDownPos.Y);
                    SelectionBox.Height = mousePos.Y - mouseDownPos.Y;
                }
                else
                {
                    Canvas.SetTop(SelectionBox, mousePos.Y);
                    SelectionBox.Height = mouseDownPos.Y - mousePos.Y;
                }
            }
        }

        public void Grid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectionBox.Visibility = Visibility.Collapsed;
            Selected.Visibility = Visibility.Collapsed;
        }

        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape))
                Application.Current.Shutdown();

            if (e.Key.Equals(Key.F))
            {
                if (MainWin.WindowState.Equals(WindowState.Normal))
                    MainWin.WindowState = WindowState.Maximized;
                else if (MainWin.WindowState.Equals(WindowState.Maximized))
                    MainWin.WindowState = WindowState.Normal;
            }

            if (e.Key.Equals(Key.T))
            {
                if (MainWin.WindowStyle.Equals(WindowStyle.SingleBorderWindow))
                    MainWin.WindowStyle = WindowStyle.None;
                else if (MainWin.WindowStyle.Equals(WindowStyle.None))
                    MainWin.WindowStyle = WindowStyle.SingleBorderWindow;
            }

            if (e.Key.Equals(Key.D0) || e.Key.Equals(Key.NumPad0))
            {
                Img.Stretch = Stretch.None;
            }
            else if (e.Key.Equals(Key.D1) || e.Key.Equals(Key.NumPad1))
            {
                Img.Stretch = Stretch.Fill;
            }
            else if (e.Key.Equals(Key.D2) || e.Key.Equals(Key.NumPad2))
            {
                Img.Stretch = Stretch.Uniform;
            }
            else if (e.Key.Equals(Key.D3) || e.Key.Equals(Key.NumPad3))
            {
                Img.Stretch = Stretch.UniformToFill;
            }
        }

        public void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Selected.Visibility == Visibility.Visible)
            {
                //double mulX = MainWin.ActualWidth / MainWin.Width;
                //double mulY = MainWin.ActualHeight / MainWin.Height;

                //Selected.Width *= mulX;
                //Selected.Height *= mulY;

                Selected.Visibility = Visibility.Collapsed;
            }
        }

        public void BlueCheckBox_CheckedChange(object sender, RoutedEventArgs e)
        {
            blueEnabled = ((CheckBox)sender).IsChecked.Value;
        }

        public void GreenCheckBox_CheckedChange(object sender, RoutedEventArgs e)
        {
            greenEnabled = ((CheckBox)sender).IsChecked.Value;
        }

        public void RedCheckBox_CheckedChange(object sender, RoutedEventArgs e)
        {
            redEnabled = ((CheckBox)sender).IsChecked.Value;
        }
    }
}
