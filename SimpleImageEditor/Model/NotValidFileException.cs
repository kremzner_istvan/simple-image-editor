﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleImageEditor.Model
{
    public class NotValidFileException: Exception
    {
        public NotValidFileException(): base("Not valid file name or extension.")
        {
        }

    }
}
