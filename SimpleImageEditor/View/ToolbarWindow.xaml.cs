﻿using SimpleImageEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleImageEditor.View
{
    /// <summary>
    /// Interaction logic for ToolbarWindow.xaml
    /// </summary>
    public partial class ToolbarWindow : Window
    {
        public ToolbarWindow(double startX, double startY)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Left = startX;
            this.Top= startY;
        }

        private void OnRestoreClick(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().RestoreImage();
        }

        private void BlueCheckBox_CheckedChange(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().BlueCheckBox_CheckedChange(sender, e);
        }

        private void GreenCheckBox_IsCheckChanged(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().GreenCheckBox_CheckedChange(sender, e);

        }

        private void RedCheckBox_IsCheckChanged(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().RedCheckBox_CheckedChange(sender, e);
        }

        private void OnAddSelectedClick(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            Viewmodel.GetViewModel().OnButtonClick(sender, e);
        }

        private void OnRemoveSelectedClick(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().OnButtonClick(sender, e);
        }

        private void OnLoadFileClick(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().OpenFileLoader();
        }

        private void OnSaveFileClick(object sender, RoutedEventArgs e)
        {
            Viewmodel.GetViewModel().SaveFile();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Viewmodel.GetViewModel().OnKeyDown(sender, e);
        }
    }
}
